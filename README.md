# XLKVMC

#### 介绍
基于三菱的MC协议，适配基恩士PLC的DM区数据交互

#### 软件架构

当前库只是拿了XL_Fins库修修改改，所以操作跟XL_Fins库一模一样，有能力的可以抽象一下，把这两个库合成一个，都是一样的接口
[XL_Fins](https://gitee.com/XingLucifer/XMFins "XL_Fins")


#### 使用说明

1.  注意：该库只针对基恩士PLC，三菱PLC也可以用，但是数据读取可能不正确