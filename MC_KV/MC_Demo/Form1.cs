using MC_Demo;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.Pkcs;
using System.Text;
using XL_KVMC;
using XL_KVMC.Protocols;

namespace TCPFins_Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Stopwatch _stopwatch = new Stopwatch();

        private async void But_Read_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                data_type = ComBox_DataType.Text;
                Txt_Log.Clear();
                SetControlEnabled(false);
                await Task.Run(() =>
                {
                    switch (button.Text)
                    {
                        case "读类":
                            try
                            {
                                _stopwatch.Restart();
                                #region 第一种方式
                                MCKVModel testModel = _fins.ReadClass<MCKVModel>(Convert.ToInt32(Txt_Address.Text));
                                #endregion
                                #region 第二种方式
                                //TestModel testModel = new TestModel();
                                //_fins.ReadClass(Convert.ToInt32(Txt_Address.Text), testModel);
                                #endregion
                                Log($"读取到的类：{System.Text.Json.JsonSerializer.Serialize(testModel)}");
                            }
                            catch (Exception ex)
                            {
                                Log($"执行[读类]方法异常：{ex}");
                            }
                            _stopwatch.Stop();
                            Log($"耗时：{_stopwatch.ElapsedMilliseconds} ms");
                            break;
                        case "读值":
                            try
                            {
                                _stopwatch.Restart();
                                Txt_Value.Invoke(() => Txt_Value.Text = GetValueConversion());
                            }
                            catch (Exception ex)
                            {
                                Log($"执行[读值]方法异常：{ex}");
                            }
                            _stopwatch.Stop();
                            Log($"耗时：{_stopwatch.ElapsedMilliseconds} ms");
                            break;
                    }
                }, _cancellation.Token);
                SetControlEnabled();
            }
        }
        private string data_type;
        private async void But_Write_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                data_type = ComBox_DataType.Text;
                Txt_Log.Clear();
                SetControlEnabled(false);
                await Task.Run(() => {
                    switch (button.Text)
                    {
                        case "写类":
                            try
                            {
                                _stopwatch.Restart();
                                byte[] barcode = Encoding.ASCII.GetBytes("0B5CB2CM91BA2JC711000028");
                                MCKVModel testModel = new MCKVModel();
                                testModel.One = 666;
                                testModel.Two = 999;
                                testModel.Three = 34.21F;
                                testModel.five = 89.63;
                                Log($"执行写入类：{System.Text.Json.JsonSerializer.Serialize(testModel)}");
                                Log($"执行状态：{_fins.WriteClass(testModel, Convert.ToInt32(Txt_Address.Text))}");
                            }
                            catch (Exception ex)
                            {
                                Log($"执行[写类]方法异常：{ex}");
                            }
                            _stopwatch.Stop();
                            Log($"耗时：{_stopwatch.ElapsedMilliseconds} ms");
                            break;
                        case "写值":
                            try
                            {
                                _stopwatch.Restart();
                                object value = ToValueConversion();
                                if (_fins.WriteSingle(value, Convert.ToInt32(Txt_Address.Text), 0, Convert.ToInt32(Txt_Length.Text)))
                                {
                                    Log("发送成功");
                                }
                                else
                                {
                                    Log("发送失败");
                                }
                            }
                            catch (Exception ex)
                            {
                                Log($"执行[写值]方法异常：{ex}");
                            }
                            _stopwatch.Stop();
                            Log($"耗时：{_stopwatch.ElapsedMilliseconds} ms");
                            break;
                    }             
                },_cancellation.Token);
                SetControlEnabled();
            }
        }
        private XL_KVMC.IDevices _fins;
        private CancellationTokenSource _cancellation;

        public bool DisconnectedCallbacks(XL_KVMC.ICommunications comm, CancellationTokenSource cancellation)
        {
            if (cancellation.Token.IsCancellationRequested)
            {
                return false;
            }
            Log("进入掉线回调...");
            comm.Close();
            Thread.Sleep(500);
            try
            {
                comm.Open();
                TCP_Fins_Protocol protocol = new TCP_Fins_Protocol();
                comm.Write(protocol.GetRegistrationMessage(_suffixIP));
                Thread.Sleep(5);
                byte[] bytes = comm.Read(24);
                int error = bytes.Skip(12).Take(4).ToArray().ByteToInt();
                if (error == 0)
                {
                    protocol.SA1 = _suffixIP;
                    protocol.DA1 = bytes.Skip(23).ToArray()[0];
                    comm.SetProtocol(protocol);
                    Log("Fins重连成功");
                    return true;
                }
                Log($"Fins回调重连异常代码：{error}");
                return false;
            }
            catch (Exception ex)
            {
                Log($"回调重连失败异常：{ex}");
                return false;
            }
        }

        private byte _suffixIP = 240;//电脑IP最后一位
        private async void But_Open_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                Txt_Log.Clear();
                SetControlEnabled(false);
                switch (button.Text)
                {
                    case "Open":
                        await Task.Run(() =>
                        {
                            Log("开始通信...");
                            if (_cancellation == null || _cancellation.Token.IsCancellationRequested)
                            {
                                _cancellation = new CancellationTokenSource();
                            }
                            var comm = new XL_KVMC.TCPBase(1, Txt_IP.Text, Convert.ToInt32(Txt_Port.Text), _cancellation, 3000, DisconnectedCallbacks);
                            _fins = new XL_KVMC.MCKV(comm);
                            try
                            {
                                _fins.Open();
                                Log("打开Fins 通信成功");
                                button.Invoke(() => button.Text = "Close");
                            }
                            catch (Exception ex)
                            {
                                Log($"打开Fins 通信失败：{ex}");
                            }
                        });
                        SetControlEnabled();
                        break;
                    case "Close":
                        _cancellation.Cancel();
                        _fins.Close();
                        SetControlEnabled();
                        button.Text = "Open";
                        break;
                }
            }
        }

        private void SetControlEnabled(bool value = true)
        {
            foreach (var item in this.Controls)
            {
                if (item is Button button)
                {
                    if (button.InvokeRequired)
                    {
                        button.Invoke(() => button.Enabled = value);
                        continue;
                    }
                    button.Enabled = value;
                }
            }
        }

        private void Log(string str)
        {
            if (Txt_Log.InvokeRequired)
            {
                Txt_Log.Invoke(() => Txt_Log.AppendText($"时间：{DateTime.Now:G}==>>{str}\r\n"));
                return;
            }
            Txt_Log.AppendText($"时间：{DateTime.Now:G}==>>{str}\r\n");
        }

        private object ToValueConversion()
        {
            try
            {
                switch (data_type)
                {
                    case "Int32": return Convert.ToInt32(Txt_Value.Text);
                    case "UInt32": return Convert.ToUInt32(Txt_Value.Text);
                    case "Single": return Convert.ToSingle(Txt_Value.Text);
                    case "Double": return Convert.ToDouble(Txt_Value.Text);
                    case "Int16": return Convert.ToInt16(Txt_Value.Text);
                    case "UInt16": return Convert.ToUInt16(Txt_Value.Text);
                    case "String": return Txt_Value.Text;
                }
            }
            catch (Exception)
            {
            }
            return (ushort)1;
        }

        private string GetValueConversion()
        {
            int address = Convert.ToInt32(Txt_Address.Text);
            switch (data_type)
            {
                case "Int32": return _fins.ReadSingle<int>(address, 0).ToString();
                case "UInt32": return _fins.ReadSingle<uint>(address, 0).ToString();
                case "Single": return _fins.ReadSingle<float>(address, 0).ToString();
                case "Double": return _fins.ReadSingle<double>(address, 0).ToString();
                case "Int16": return _fins.ReadSingle<short>(address, 0).ToString();
                case "UInt16": return _fins.ReadSingle<ushort>(address, 0).ToString();
                case "String": return _fins.ReadSingle<string>(address, Convert.ToInt32(Txt_Length.Text)).ToString();
            }
            return "";
        }
    }
}