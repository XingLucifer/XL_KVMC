﻿namespace TCPFins_Demo
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.But_Open = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_IP = new System.Windows.Forms.TextBox();
            this.Txt_Port = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ComBox_DataType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Value = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_Address = new System.Windows.Forms.TextBox();
            this.But_Read = new System.Windows.Forms.Button();
            this.But_Write = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Log = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Length = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // But_Open
            // 
            this.But_Open.Location = new System.Drawing.Point(126, 38);
            this.But_Open.Name = "But_Open";
            this.But_Open.Size = new System.Drawing.Size(75, 23);
            this.But_Open.TabIndex = 0;
            this.But_Open.Text = "Open";
            this.But_Open.UseVisualStyleBackColor = true;
            this.But_Open.Click += new System.EventHandler(this.But_Open_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP：";
            // 
            // Txt_IP
            // 
            this.Txt_IP.Location = new System.Drawing.Point(52, 9);
            this.Txt_IP.Name = "Txt_IP";
            this.Txt_IP.Size = new System.Drawing.Size(149, 23);
            this.Txt_IP.TabIndex = 2;
            this.Txt_IP.Text = "192.168.20.110";
            // 
            // Txt_Port
            // 
            this.Txt_Port.Location = new System.Drawing.Point(52, 38);
            this.Txt_Port.Name = "Txt_Port";
            this.Txt_Port.Size = new System.Drawing.Size(61, 23);
            this.Txt_Port.TabIndex = 4;
            this.Txt_Port.Text = "5000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port：";
            // 
            // ComBox_DataType
            // 
            this.ComBox_DataType.FormattingEnabled = true;
            this.ComBox_DataType.Items.AddRange(new object[] {
            "Int32",
            "UInt32",
            "Single",
            "Double",
            "Int16",
            "UInt16",
            "String"});
            this.ComBox_DataType.Location = new System.Drawing.Point(334, 9);
            this.ComBox_DataType.Name = "ComBox_DataType";
            this.ComBox_DataType.Size = new System.Drawing.Size(100, 25);
            this.ComBox_DataType.TabIndex = 5;
            this.ComBox_DataType.Text = "Int16";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(260, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "数据类型：";
            // 
            // Txt_Value
            // 
            this.Txt_Value.Location = new System.Drawing.Point(334, 69);
            this.Txt_Value.Name = "Txt_Value";
            this.Txt_Value.Size = new System.Drawing.Size(100, 23);
            this.Txt_Value.TabIndex = 7;
            this.Txt_Value.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "数值：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "地址：";
            // 
            // Txt_Address
            // 
            this.Txt_Address.Location = new System.Drawing.Point(334, 40);
            this.Txt_Address.Name = "Txt_Address";
            this.Txt_Address.Size = new System.Drawing.Size(100, 23);
            this.Txt_Address.TabIndex = 9;
            this.Txt_Address.Text = "2000";
            // 
            // But_Read
            // 
            this.But_Read.Location = new System.Drawing.Point(455, 40);
            this.But_Read.Name = "But_Read";
            this.But_Read.Size = new System.Drawing.Size(75, 23);
            this.But_Read.TabIndex = 11;
            this.But_Read.Text = "读类";
            this.But_Read.UseVisualStyleBackColor = true;
            this.But_Read.Click += new System.EventHandler(this.But_Read_Click);
            // 
            // But_Write
            // 
            this.But_Write.Location = new System.Drawing.Point(455, 69);
            this.But_Write.Name = "But_Write";
            this.But_Write.Size = new System.Drawing.Size(75, 23);
            this.But_Write.TabIndex = 12;
            this.But_Write.Text = "写类";
            this.But_Write.UseVisualStyleBackColor = true;
            this.But_Write.Click += new System.EventHandler(this.But_Write_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(536, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "读值";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.But_Read_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(536, 69);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "写值";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.But_Write_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "运行日志";
            // 
            // Txt_Log
            // 
            this.Txt_Log.Location = new System.Drawing.Point(12, 98);
            this.Txt_Log.Multiline = true;
            this.Txt_Log.Name = "Txt_Log";
            this.Txt_Log.Size = new System.Drawing.Size(599, 343);
            this.Txt_Log.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(461, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "字符长度：";
            // 
            // Txt_Length
            // 
            this.Txt_Length.Location = new System.Drawing.Point(536, 9);
            this.Txt_Length.Name = "Txt_Length";
            this.Txt_Length.Size = new System.Drawing.Size(75, 23);
            this.Txt_Length.TabIndex = 17;
            this.Txt_Length.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 450);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_Length);
            this.Controls.Add(this.Txt_Log);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.But_Write);
            this.Controls.Add(this.But_Read);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Txt_Address);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Txt_Value);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ComBox_DataType);
            this.Controls.Add(this.Txt_Port);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_IP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.But_Open);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KVMC Demo-----------------------作者：野原新之助丶今年五岁";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button But_Open;
        private Label label1;
        private TextBox Txt_IP;
        private TextBox Txt_Port;
        private Label label2;
        private ComboBox ComBox_DataType;
        private Label label3;
        private TextBox Txt_Value;
        private Label label4;
        private Label label5;
        private TextBox Txt_Address;
        private Button But_Read;
        private Button But_Write;
        private Button button1;
        private Button button2;
        private Label label6;
        private TextBox Txt_Log;
        private Label label7;
        private TextBox Txt_Length;
    }
}