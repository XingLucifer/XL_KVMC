﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_KVMC
{
    public static class ByteHelper
    {

        /// <summary>
        /// 字节转整形
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static int ByteToInt(this byte[] bytes) => (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + bytes[3];
    }
}
