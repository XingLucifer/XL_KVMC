﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace XL_KVMC
{
    public interface ICommunications
    {
        /// <summary>
        /// 设备IP或串口
        /// </summary>
        string IP { get; }
        /// <summary>
        /// 端口或波特率
        /// </summary>
        int Port { get; }
        /// <summary>
        /// 索引
        /// </summary>
        int Index { get; }
        /// <summary>
        /// 超时时间
        /// </summary>
        int Timeout { get; set; }
        void Write(byte[] bytes);
        byte[] Read(int length);
        byte[] SendToRead(byte[] bytes, int count);
        bool Open();
        void Close();
        void SetProtocol(IProtocol protocol);
    }
}
