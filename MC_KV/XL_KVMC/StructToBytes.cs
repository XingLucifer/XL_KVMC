﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace XL_KVMC
{
    public static class StructToBytes
    {
        private static IEnumerable<PropertyInfo> GetAccessableProperties(Type classType)
        {
            return classType
#if NETSTANDARD1_3
                .GetTypeInfo().DeclaredProperties.Where(p => p.SetMethod != null);
#else
                .GetProperties(
                    BindingFlags.SetProperty |
                    BindingFlags.Public |
                    BindingFlags.Instance)
                .Where(p => p.GetSetMethod() != null);
#endif

        }

        private static double GetIncreasedNumberOfBytes(double numBytes, Type type)
        {
            switch (type.Name)
            {
                case "Boolean":
                    numBytes += 0.125;
                    break;
                case "Byte":
                    numBytes = Math.Ceiling(numBytes);
                    numBytes++;
                    break;
                case "Int16":
                case "UInt16":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    numBytes += 2;
                    break;
                case "Int32":
                case "UInt32":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    numBytes += 4;
                    break;
                case "Single":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    numBytes += 4;
                    break;
                case "Double":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    numBytes += 8;
                    break;
                default:
                    var propertyClass = Activator.CreateInstance(type);
                    numBytes = GetClassSize(propertyClass, numBytes, true);
                    break;
            }

            return numBytes;
        }

        public static double GetClassSize(object instance, double numBytes = 0.0, bool isInnerProperty = false)
        {
            var properties = GetAccessableProperties(instance.GetType());
            foreach (var property in properties)
            {
                if (property.PropertyType.IsArray)
                {
                    Type elementType = property.PropertyType.GetElementType();
                    Array array = (Array)property.GetValue(instance, null);
                    if (array.Length <= 0)
                    {
                        throw new Exception("Cannot determine size of class, because an array is defined which has no fixed size greater than zero.");
                    }

                    IncrementToEven(ref numBytes);
                    for (int i = 0; i < array.Length; i++)
                    {
                        numBytes = GetIncreasedNumberOfBytes(numBytes, elementType);
                    }
                }
                else
                {
                    numBytes = GetIncreasedNumberOfBytes(numBytes, property.PropertyType);
                }
            }
            if (false == isInnerProperty)
            {
                // enlarge numBytes to next even number because S7-Structs in a DB always will be resized to an even byte count
                numBytes = Math.Ceiling(numBytes);
                if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                    numBytes++;
            }
            return numBytes;
        }

        public static double FromBytes(object sourceClass, byte[] bytes, ref int count, double numBytes = 0)
        {
            if (bytes == null)
                return numBytes;

            var properties = GetAccessableProperties(sourceClass.GetType());
            foreach (var property in properties)
            {
                if (property.PropertyType.IsArray)
                {
                    Array array = (Array)property.GetValue(sourceClass, null);
                    IncrementToEven(ref numBytes);
                    Type elementType = property.PropertyType.GetElementType();
                    for (int i = 0; i < array.Length && numBytes < bytes.Length; i++)
                    {
                        array.SetValue(GetPropertyValue(elementType, bytes, ref numBytes, ref count), i);
                    }
                }
                else
                {
                    property.SetValue(
                        sourceClass,
                        GetPropertyValue(property.PropertyType, bytes, ref numBytes, ref count),
                        null);
                }
                if (count == 16 || property.PropertyType.Name != "Boolean")
                {
                    count = 0;
                }
            }

            return numBytes;
        }

        public static double ToBytes(object sourceClass, byte[] bytes, ref int count, double numBytes = 0.0)
        {
            var properties = GetAccessableProperties(sourceClass.GetType());
            foreach (var property in properties)
            {
                if (property.PropertyType.IsArray)
                {
                    Array array = (Array)property.GetValue(sourceClass, null);
                    IncrementToEven(ref numBytes);
                    Type elementType = property.PropertyType.GetElementType();
                    for (int i = 0; i < array.Length && numBytes < bytes.Length; i++)
                    {
                        numBytes = SetBytesFromProperty(array.GetValue(i), bytes, numBytes, ref count);
                    }
                }
                else
                {
                    numBytes = SetBytesFromProperty(property.GetValue(sourceClass, null), bytes, numBytes, ref count);
                }
                if (count == 16 || property.PropertyType.Name != "Boolean")
                {
                    count = 0;
                }
            }
            return numBytes;
        }

        public static void IncrementToEven(ref double numBytes)
        {
            numBytes = Math.Ceiling(numBytes);
            if (numBytes % 2 > 0) numBytes++;
        }

        public static void GetBytes(object propertyValue, byte[] bytes, double numBytes = 0.0) => SetBytesFromProperty(propertyValue, bytes, numBytes, ref _count);
        private static int _count = 0;
        public static object GetValue(Type source, byte[] bytes, double numBytes = 0) => GetPropertyValue(source, bytes, ref numBytes, ref _count);

        private static object GetPropertyValue(Type propertyType, byte[] bytes, ref double numBytes, ref int count)
        {
            object? value = null;
            switch (propertyType.Name)
            {
                case "Boolean":
                    count++;
                    int bytePos = (int)Math.Floor(numBytes);
                    int bitPos = (int)((numBytes - (double)bytePos) / 0.125);
                    if (count > 8)
                    {
                        value = (bytes[bytePos - 1] & (int)Math.Pow(2, bitPos)) != 0;
                    }
                    else
                    {
                        value = (bytes[bytePos + 1] & (int)Math.Pow(2, bitPos)) != 0;
                    }
                    numBytes += 0.125;
                    break;
                case "Byte":
                    numBytes = Math.Ceiling(numBytes);
                    value = (byte)(bytes[(int)numBytes]);
                    numBytes++;
                    break;
                case "Int16":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    // hier auswerten
                    value = BitConverter.ToInt16(new byte[] { bytes[(int)numBytes], bytes[(int)numBytes + 1] });
                    numBytes += 2;
                    break;
                case "UInt16":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    // hier auswerten
                    value = BitConverter.ToUInt16(new byte[] { bytes[(int)numBytes], bytes[(int)numBytes + 1] });
                    numBytes += 2;
                    break;
                case "Int32":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    value = BitConverter.ToInt32(
                        new byte[] {
                            bytes[(int)numBytes],
                            bytes[(int)numBytes + 1],
                            bytes[(int)numBytes + 2],
                            bytes[(int)numBytes + 3]
                        });
                    numBytes += 4;
                    break;
                case "UInt32":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    // hier auswerten
                    value = BitConverter.ToUInt32(
                        new byte[] {
                            bytes[(int)numBytes],
                            bytes[(int)numBytes + 1],
                            bytes[(int)numBytes + 2],
                            bytes[(int)numBytes + 3]
                        });
                    numBytes += 4;
                    break;
                case "Single":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    value = BitConverter.ToSingle(
                        new byte[] {bytes[(int)numBytes], 
                            bytes[(int)numBytes + 1], 
                            bytes[(int)numBytes + 2], 
                            bytes[(int)numBytes + 3] });
                    numBytes += 4;
                    break;
                case "Double":
                    numBytes = Math.Ceiling(numBytes);
                    if ((numBytes / 2 - Math.Floor(numBytes / 2.0)) > 0)
                        numBytes++;
                    var buffer = new byte[8];
                    buffer[0] = bytes[(int)numBytes];
                    buffer[1] = bytes[(int)numBytes + 1];
                    buffer[2] = bytes[(int)numBytes + 2];
                    buffer[3] = bytes[(int)numBytes + 3];
                    buffer[4] = bytes[(int)numBytes + 4];
                    buffer[5] = bytes[(int)numBytes + 5];
                    buffer[6] = bytes[(int)numBytes + 6];
                    buffer[7] = bytes[(int)numBytes + 7];
                    //Array.Copy(bytes, (int)numBytes, buffer, 0, 8);

                    value = BitConverter.ToDouble(buffer);
                    numBytes += 8;
                    break;
                default:
                    var propClass = Activator.CreateInstance(propertyType);
                    numBytes = FromBytes(propClass, bytes, ref count, numBytes);
                    value = propClass;
                    break;
            }

            return value;
        }

        private static double SetBytesFromProperty(object propertyValue, byte[] bytes, double numBytes, ref int count)
        {
            int bytePos = 0;
            int bitPos = 0;
            byte[]? bytes2 = null;

            switch (propertyValue.GetType().Name)
            {
                case "Boolean":
                    count++;
                    bytePos = (int)Math.Floor(numBytes);
                    bitPos = (int)((numBytes - (double)bytePos) / 0.125);
                    int myBytePos = count > 8 ? bytePos - 1 : bytePos + 1;
                    if ((bool)propertyValue)
                        bytes[myBytePos] |= (byte)Math.Pow(2, bitPos);
                    else
                        bytes[myBytePos] &= (byte)(~(byte)Math.Pow(2, bitPos));
                    numBytes += 0.125;
                    break;
                case "Byte":
                    numBytes = (int)Math.Ceiling(numBytes);
                    bytePos = (int)numBytes;
                    bytes[bytePos] = (byte)propertyValue;
                    numBytes++;
                    break;
                case "Int16":
                    bytes2 = BitConverter.GetBytes((short)propertyValue);
                    break;
                case "UInt16":
                    bytes2 = BitConverter.GetBytes((ushort)propertyValue);
                    break;
                case "Int32":
                case "UInt32":
                case "Single":
                    switch (propertyValue.GetType().Name)
                    {
                        case "Single":
                            bytes2 = BitConverter.GetBytes((float)propertyValue);
                            break;
                        case "Int32":
                            bytes2 = BitConverter.GetBytes((Int32)propertyValue);
                            break;
                        case "UInt32":
                            bytes2 = BitConverter.GetBytes((UInt32)propertyValue);
                            break;
                    }
                    byte transit = bytes2[1];
                    bytes2[0] = bytes2[0];
                    bytes2[1] = transit;
                    transit = bytes2[3];
                    bytes2[2] = bytes2[2];
                    bytes2[3] = transit;
                    break;
                case "Int64":
                case "UInt64":
                case "Double":
                    switch (propertyValue.GetType().Name)
                    {
                        case "Double":
                            bytes2 = BitConverter.GetBytes((double)propertyValue);
                            break;
                        case "UInt64":
                            bytes2 = BitConverter.GetBytes((UInt64)propertyValue);
                            break;
                        case "Int64":
                            bytes2 = BitConverter.GetBytes((Int64)propertyValue);
                            break;
                    }
                    break;
                default:
                    numBytes = ToBytes(propertyValue, bytes, ref count, numBytes);
                    break;
            }

            if (bytes2 != null)
            {
                StructToBytes.IncrementToEven(ref numBytes);

                bytePos = (int)numBytes;
                for (int bCnt = 0; bCnt < bytes2.Length; bCnt++)
                    bytes[bytePos + bCnt] = bytes2[bCnt];
                numBytes += bytes2.Length;
            }

            return numBytes;
        }
    }
}
