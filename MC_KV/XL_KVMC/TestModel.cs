﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_KVMC
{
    public class TestModel
    {
        public TestModel()
        {
            Cmd = new CmdModel();
            Barcode = new byte[32];
        }

        public float PressurizationUP { get; set; }
        public CmdModel Cmd { get; set; }
        public byte[] Barcode { get; set; }
        public ushort Index { get; set; }
    }

    public class CmdModel
    {
        public ushort One { get; set; }
        public ushort Two { get; set; }
        public ushort Three { get; set; }
        public ushort Four { get; set; }
    }
}
