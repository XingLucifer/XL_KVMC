﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_KVMC
{
    public interface IProtocol
    {
        public bool Verify(IList<byte> bytes);
        public byte[] Serialize(IList<byte> data);
        public byte[] Deserialize(IList<byte> data);
    }
}
