﻿using System;
using System.Threading.Tasks;

namespace XL_KVMC
{
    public interface IDevices
    {
        bool Open();
        void Close();
        Task Read();
        byte[] Read(int length);
        byte[] Write(byte[] buffer);
        bool WriteSingle(object value, int address, int offset, int length, int count = 3);
        bool WriteClass<TClass>(TClass value, int address, int count = 3) where TClass : class;
        TValue ReadSingle<TValue>(int address, int length, int count = 3);
        TClass? ReadClass<TClass>(int address, TClass obj = default, int count = 3) where TClass : class;
    }
}
